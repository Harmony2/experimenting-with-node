# Experimenting with node

Some projects to mess around and learn some node through the best possible way: Experimentation

# Environment with Docker

Instead of installing Node in my machine, I (for now, at least) rather use Docker. This way i still can develop in the same way i would,
but i'm able to isolate my environment. Therefore I don't have to deal with any installations and management on my host machine
and still mantain every dependency I need.

This command will mount the repo on the container, this way you can share the files hosted on the machine with the container itself in realtime.
This allows you to use IDE's and manipulate the files on your host machine, and run them with Node from inside the container.

    docker run --name docker-runner -ti -p 8080:80 --mount type=bind,source="$(pwd)",target=/app node:16-alpine3.11 /bin/ash

Happy Coding!