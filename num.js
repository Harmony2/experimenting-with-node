// Just my classic Num class example. This is a nice excercise to just get a grip on the object orientation aspect of the language
// This can, and should also be used outside of this scope. Try consuming this as a module with an API, for example!

class Num {
    constructor(x) {
        this.x = x;
        this.isEven = this.checkEven();
        this.isPrime = this.checkPrime();
    };

    checkEven() {
        if (this.x % 2 == 0) {
            return true;
        }
        else {
            return false;
        }
    };

    checkPrime() {
        if (this.x == 2) {
            return true;
        }
        else if (this.isEven && this.x != 2) {
            return false;
        };
        var dividers = 0;
        for (var i=1; i < this.x; i++) {
            if (this.x % i == 0) {
                dividers ++;
                if (dividers >= 2) {return false};
            }
        }
        return true;
    };

    add(y) {
        return this.x + y;
    }

    subtract(y) {
        return this.x - y;
    }

    divide(y) {
        return this.x / y;
    }

    multiply(y) {
        return this.x * y;
    }

    potentiate(y) {
        return this.x ** y;
    }
};

// for (var i=0; i < 100; i++) {
//     num = new Num(i);
//     console.log(
//         "is ", num.x, " even?", num.isEven
//     );
//     console.log(
//         "is ", num.x, " prime?", num.isPrime, "\n"
//     );
// };

// num = new Num(5);

// console.log(
//     num.add(12)
// );

// console.log(
//     num.subtract(2)
// );

// console.log(
//     num.multiply(5)
// );

// console.log(
//     num.divide(5)
// );

// console.log(
//     num.potentiate(5)
// );
